function createCircle() {
    let circleEl = document.createElement('div') // crea un nuevo elemento tipo div
	circleEl.classList.add('circle') //llame a la clase circle
	circleEl.style.top = Math.random() * innerHeight + "px"; //agrega un circulo en relación a la altura de la pantalla
	circleEl.style.left = Math.random() * innerWidth + "px"; //agrega un circulo en relación a lo ancho de la pantalla
	document.body.appendChild(circleEl); //agrega la variable creada

	setTimeout(() => { //retrasa la ejecución de mi variable y despues la elimina
        circleEl.remove()
    }, 3000)
}

setInterval(createCircle, 300) //repetir la función de mi variable, aquí vuelven a aparecer
